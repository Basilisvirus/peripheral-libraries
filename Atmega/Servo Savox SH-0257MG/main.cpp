/*
 * lm1085.c
 *
 * Created: 9/21/2020 6:14:55 AM
 * Author : Christianidis Vasileios
 * control savox on PD5
 */ 
#define F_CPU 8000000UL //the clock of the cpu (internal is 8 000 000 hz)
#define BRC ((F_CPU/16/9600)-1) //define the baud rate (usually 9600)

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <util/delay.h>

#define bit_get(p,m) ((p) & (m)) //if(bit_get(foo, BIT(3)))
#define bit_set(p,m) ((p) |= (m)) //bit_set(foo, 0x01); bit_set(foo, BIT(5));
#define bit_clear(p,m) ((p) &= ~(m)) //bit_clear(foo, 0x40);
#define bit_flip(p,m) ((p) ^= (m)) //bit_flip(foo, BIT(0));
#define bit_write(c,p,m) (c ? bit_set(p,m) : bit_clear(p,m)) //bit_write(bit_get(foo, BIT(4)), bar, BIT(0)); (To set or clear a bit based on bit number 4:)
#define BIT(x) (0x01 << (x)) // 0<x<7
#define LONGBIT(x) ((unsigned long)0x00000001 << (x))


//TX functions
void sw(char c[]); //print a string on serial
void swn(int num, int type, int ln); //print a Register or a number (no float), in hex(2) or in decimal(10)
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it); //Take a float and print it on serial

//RX functions
void readUntill(char c);

//======================TX START======================
volatile uint8_t len, k=0; //unsigned 8 bit integer
volatile char str[40], str_floatx[40];
//======================TX END=======================

//======================RX START=======================
char rxBuffer[128]; //the user input from 'readuntill() is saved in rxBuffer[128].
volatile char udr0 = '\0', rx_stop='\0'; /*  NULL = \0  */ //udr0 is where the input from the user is saved
volatile uint8_t rxReadPos = 0, rxWritePos = 0, readString =0;
//======================RX END=======================


//functions
void main_function(); //initializes the mcu, the first code that runs on main
void init_rxtx_function(); //initializes only rxtx functionality
void init_function(); //this goes in while(1) as a loop, repeats itself

//VARS
volatile uint8_t ocr0b_setting, ocr0b_setting_for_change, ready_for_ocr0b_change=0;
volatile char user_input = '\0';
volatile uint8_t first_in = 0;

int main(void)
{
	init_rxtx_function();
	init_function();

    while (1) 
    {
		main_function();
    }
}

void main_function()//this goes in while(1) as a loop, repeats itself
{
	user_input = udr0; //take the input from the serial.
	if(user_input == 's')
	{
	
		sw("give new ocr0b value: ");
		readUntill('s'); //read the new value. new value is saved in 'rxBuffer'
		sw("first_in is: ");
		swn(first_in, 10, 1);
		
		ocr0b_setting_for_change = atoi(rxBuffer); //save the value to be changes in this variable
		ready_for_ocr0b_change = 1; //change the flag. the new value is ready to be written to the ocr0b_setting.
	}
	
}


void init_function()//initializes the mcu, the first code that runs on main
{
	sw("start CTC 3.1 \n\r");
	
	SREG   |= 0B10000000;
	
	//256 PRESCALER
	bit_set(TCCR0B, BIT(2)); //CS02=1
	bit_clear(TCCR0B, BIT(1)); //CS01=0
	bit_clear(TCCR0B, BIT(0)); //CS00=0

	//CTC MODE
	bit_clear(TCCR0A, BIT(0)); //WGM00=0
	bit_set(TCCR0A, BIT(1)); //WGM01=1
	bit_clear(TCCR0B, BIT(3)); //WGM02=0
	
	//MODE for OC0A PD6 (disconnect)
	bit_clear(TCCR0A, BIT(7));//COM0A1=0;
	bit_clear(TCCR0A,BIT(6)); //COM0A0=1;
	
	//MODE for OC0B PD5 (toggle on compare match)
	bit_clear(TCCR0A, BIT(5));//COMOB1=0;
	bit_set(TCCR0A, BIT(4));//COMOB0=1;
	
	OCR0B = 54; //4<OCR0B<54
	OCR0A = 72; 
	
	//ENABLE SPECIFIC INTERRUPTS (ALL)
	bit_set(TIMSK0, BIT(2));
	bit_set(TIMSK0, BIT(1));
	bit_set(TIMSK0, BIT(0));
	
	//OC0A, OC0B OUTPUT
	bit_set(DDRD, BIT(5));
	bit_set(DDRD, BIT(6));
	
	ocr0b_setting = OCR0B;
}

/*
 OCR0B value CONTROLS THE PWM, AND IT IS CHANGED AS NEEDED.
 each and everytime ocr0b is changed, the first 'if' that needs to be triggered, is the 'else' statement
this means, that ocr0b should be initialized at the higher value (OCR0B >4) 
The limits for OCR0B are 4<OCR0B<54 (NO 4 OR 54)
*/
ISR(TIMER0_COMPB_vect){
	if(OCR0B==4)//leave the 4 as default and unchanged.
	{
		OCR0B = ocr0b_setting;
	}
	else //if ocr0b gets a new value, this statement should run first. Values of 54 or higher are prohibited.
	{
		OCR0B = 4; //leave the 4 as default and unchanged.
	}
}


ISR(TIMER0_OVF_vect)
{

}

/*
When chaning the value of OCR0B, the new value should be used at a specific time, not in the while loop.
On compare with OCR0A, the value of ocr0b_setting changes. The ocr0b is used to change the value of OCR0B. 
*/

ISR(TIMER0_COMPA_vect)
{
	if(ready_for_ocr0b_change==1)//this flag changes in while, it indicates that a new value is ready to be written.
	{
		ocr0b_setting=ocr0b_setting_for_change; //change the ocr0b_setting value
		ready_for_ocr0b_change=0; //reset the flag.
	}
	
}



void init_rxtx_function()
{
	//==================================TX START====================================
	UBRR0H = (BRC >> 8); //Put BRC to UBRR0H and move it right 8 bits.
	UBRR0L = BRC;
	UCSR0B = (1 << TXEN0); //Trans enable
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); //8 BIT data frame

	//ENABLE interrupts
	sei();
	//==================================TX END=====================================
	//================================== RX START===========================================
	UCSR0B |= (1 << RXEN0) | (1 << RXCIE0); //RX enable and Receive complete interrupt enable
	//================================== RX END=============================================
}

void sw(char toWrite[]){
	len = strlen(toWrite); //take size of characters to be printed
	k=0;	//initialize i
	UDR0=0; //make sure UDR0 is 0
	
	while (k<len) //while i is less than the total length of the characters to be printed
	{
		if (  ((UCSR0A & 0B00100000) == 0B00100000)  ){ //if UDRE0 is 1 (aka UDR0 is ready to send)
			UDR0 = toWrite[k]; //put the next character to be sent (now, UDRE0 is 0)
			k++;		//increase the position, and wait until UDRE0 is 1 again
		}
	}
	udr0 = '\0';
}

void swn(int num, int type, int ln) //take an int and print it on serial
{
	char str_intx[50];//declare a string of 50 characters
	
	itoa(num, str_intx, type);//convert from int to char and save it on str
	
	sw(str_intx); //serial write str
	
	if(ln == 1) //if we want a new line,
	{
		sw("\n\r");
	}
	
}

/*
Input: float number, output: print float number to serial, OR only return the float as string.
*/
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it) //Take a float and print it on serial
{

	if(extra_accur == 1)
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 1000)));
	}
	else
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 100))); //edited to accept negative numbers
	}

	//sprintf(str_floatx,"%d.%02u", (int) numf, (int) ((numf - (int) numf ) * 100) ); //this was te original function

	if(print_it)
	{
		sw(str_floatx);	
		
		if(lnf == 1) //if we want a new line,
		{
			sw("\n\r");
		}
	}
}

//==================================TX END=====================================

void readUntill(char c_rx)
{
	rxWritePos = 0;//begin writing in the buffer from pos 0
	readString = 1;//interrupt will read and save strings
	rx_stop = c_rx; //interrupt will use the global var rx_stop to stop reading the string
	
	do{
		
	}while(readString == 1);
}

ISR(USART_RX_vect)
{
	if(readString == 1)
	{	
		rxBuffer[rxWritePos] = UDR0;
		if(rxBuffer[rxWritePos] == rx_stop)
		{
			readString = 0;
			/*when you initialize a character array by listing all of its characters 
			separately then you must supply the '\0' character explicitly */
			rxBuffer[rxWritePos] = '\0' ;
		}
		rxWritePos++;
	}
	else
	{
		udr0 = UDR0;	
	}
}

//==================================RX END==================================

