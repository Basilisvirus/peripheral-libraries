/*
ADC triangle
 */ 

#include <stdio.h>
#include <stdlib.h>

#define F_CPU 8000000UL //the clock of the cpu (internal is 8 000 000 hz)
#define BRC ((F_CPU/16/9600)-1) //define the baud rate (usually 9600)

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <util/delay.h>


#define BIT_GET(p,m) ((p) & (m)) //if(bit_get(foo, BIT(3)))
#define BIT_SET(p,m) ((p) |= (m)) //bit_set(foo, 0x01); bit_set(foo, BIT(5));
#define BIT_CLEAR(p,m) ((p) &= ~(m)) //bit_clear(foo, 0x40);
#define BIT_FLIP(p,m) ((p) ^= (m)) //bit_flip(foo, BIT(0));
#define BIT_WRITE(c,p,m) (c ? bit_set(p,m) : bit_clear(p,m)) //bit_write(bit_get(foo, BIT(4)), bar, BIT(0)); (To set or clear a bit based on bit number 4:)
#define BIT(x) (0x01 << (x)) // 0<x<7
#define LONGBIT(x) ((unsigned long)0x00000001 << (x))


//TX functions
void sw(const char c[]); //print a string on serial
void swn(int num, int type, int ln); //print a Register or a number (no float), in hex(2) or in decimal(10)
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it); //Take a float and print it on serial

//RX functions
void readUntill(char c);

//======================TX START======================
volatile uint8_t len, k=0; //unsigned 8 bit integer
volatile char str[40], str_floatx[40];
//======================TX END=======================

//======================RX START=======================
char rxBuffer[128];
volatile char udr0 = '\0', rx_stop='\0'; /*  NULL = \0  */
volatile uint8_t rxReadPos = 0, rxWritePos = 0, readString =0;
//======================RX END=======================

//Functions
void init_rxtx_function();
void FastMode(uint16_t DAC_Out, uint8_t stop);
void WriteDacReg(uint16_t DAC_Out, uint8_t WriteEeprom, uint8_t stop);
uint8_t ReadDacReg (uint8_t readyOnly, uint8_t stop);

//Vars
int loopRd = 0;

void TWI_Init(){
	TWBR = 8; //Set bit rate (0x0C=12 , OPT:8)
}

void TWI_Start(){
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTA);// (1 << TWINT) makes twint 0
	/*Immediately after this operation, TWINT is set to 0, a Start is issued
	 untill the transfer is done. When transfer is done, TWINT becomes 1*/
	while((TWCR&(0B10000000))==0);//while TWINT =0 do nothing. the TWINT will be 1 when start is done.
	/* This indicates that the start conition has been transmitted 
	Alternative: while (!(TWCR & (1<<TWINT)));*/
	if ((TWSR & 0xF8) != 0x08)//if Start condition has not been transmitted
	{
		sw("TWSR!=0x08\n\r"); //print an error
	}
	//while((TWSR & (0xF8))!= 0x08); //0x08 = 11111000 //A START condition has been transmitted
}

void TWI_Write_Addr_Write(unsigned char Addr){
	TWDR = Addr; //write the address + WRITE (SLA+W) to be sent
	TWCR = (1 << TWINT) | (1 << TWEN); //Enable TWINT(make it 0) to execute command, Enable TWI.
	while((TWCR& (0B10000000))==0);//while TWINT =0 do nothing. the TWINT will be 1 when address transfer is done.
	while((TWSR&(0xF8)) != 0x18); //0x18 = 11000 //SLA+W has been transmitted; ACK has been received
}

void TWI_Write_Data(uint8_t Data){
	TWDR = Data; //data to be sent
	TWCR = (1 << TWINT) | (1 << TWEN); //Execute command (TWINT), enable TWI (TWEN).
	while((TWCR&(0B10000000)) == 0); //while TWINT =0 do nothing. the TWINT will be 1 when data transfer is done.
	while((TWSR&(0xF8)) != 0x28); //Data byte has been transmitted; ACK has been received
}

void TWI_Stop(){
	TWCR = (1 << TWINT)|(1 << TWSTO); //TWI stop and TWI interrupt enable to execute command
}

//MASTER RECEIVER
/*
After TWI_Start, if we need to read from the Slave transmitter, we use the below function.
Read result is in TWDR.
*/
void TWI_Write_Addr_Read(unsigned char Addr)
{
	TWDR = Addr; //write the address + WRITE (SLA+W) to be sent
	TWCR = (1 << TWINT) | (1 << TWEN); //Enable TWINT(make it 0) to execute command, Enable TWI.
	while((TWCR& (0B10000000))==0);//while TWINT =0 do nothing. the TWINT will be 1 when address transfer is done.
	while((TWSR&(0xF8)) != 0x40); //0x40 = 1000000 //SLA+R has been transmitted; ACK has been received
}

void TWI_Write_Addr_Read_Repeatedly()
{
	TWCR = (1 << TWINT) | (1 << TWEA)| (1 << TWEN); //Enable TWINT(make it 0) to execute command, Data byte will be received and ACK will be returned
	//waiting for all data to be read
	while((TWCR&(0B10000000)) == 0); //while TWINT =0 do nothing. the TWINT will be 1 when data transfer (read) is done.
	//Data byte has been received and ACK has been returned
	while((TWSR&(0xF8)) != 0x50); //0x50 = 1010000
}

//I2C variables
uint16_t Vout = 0; //binary value going into register, 0-4095

int main(void)
{
	init_rxtx_function();
	sw("------------- 28\n\r");
	int up=1;	
	sei();
	TWI_Init();//Set bit rate

	TWI_Start();//Start condition
	TWI_Write_Addr_Write(0B11000000);//Call the slave device for write to Register
	WriteDacReg(0B111111111111, 1, 0);//Write both DAC & EEPROM

	TWI_Stop();

	TWI_Start();//Start condition
	TWI_Write_Addr_Read(0B11000001);//Call the slave device for read.
	ReadDacReg(0,1);//Read all the contents of the DAC reg.

	//TWI_Start();//Start condition
	//TWI_Write_Addr_Write(0B11000000);//Call the slave device for write to Register
	//FastMode(0B111111111111, 1);//Write to register the value of 4095 and stop.

	while(1)
	{
		if(up==1)
		{
			Vout++;
				if (Vout == 4095)
				{
					up=0;
				}
		}
		else if (up==0)
		{
			Vout--;
				if (Vout == 0)
				{
					up=1;
				}
		}
		_delay_us(100);
	}
}

/*
After address acknowledge.
This loop can be repeated if no stop condition is used.
serial writes delay the output speed.
*/
void FastMode(uint16_t DAC_Out, uint8_t stop)
{
	//FIRST:
	//TWI_Start();//Start condition
	//TWI_Write_Addr_Write(0B11000000);//Call the slave device for write to Register
	
	uint8_t lVout = DAC_Out;
	uint8_t hVout = (DAC_Out>>8) & 0x0F;

	swn(hVout, 2, 1);
	swn(lVout, 2, 1);
		
	TWI_Write_Data(hVout);//Send high data
	TWI_Write_Data(lVout);//Send low data

	//If we need a stop condition.
	if(stop == 1)
	{
		TWI_Stop(); //Send stop condition
	}
}

/*
If WriteEeprom = 0, then this function is the same as FastMode.
If WriteEeprom = 1, then the Eeprom is also written.
*/
void WriteDacReg(uint16_t DAC_Out, uint8_t WriteEeprom, uint8_t stop)
{
	//FIRST:
	//TWI_Start();//Start condition
	//TWI_Write_Addr_Write(0B11000000);//Call the slave device for write to Register

	uint8_t hVout = DAC_Out >> 4;
	uint8_t lVout = (DAC_Out<<4);

	if(WriteEeprom == 0)//Just DAC write
	{
		TWI_Write_Data(0B01000000); //C2 = 0, C1 = 1, C0 = 0 (WRITE DAC ONLY)
		TWI_Write_Data(hVout);
		TWI_Write_Data(lVout);
		//If we need a stop condition.
		if(stop == 1)
		{
			TWI_Stop(); //Send stop condition
		}
	}
	else if(WriteEeprom == 1)//Also write EEPROM
	{
		uint8_t DacRdy = 0;
		TWI_Write_Data(0B01100000); //C2 = 0, C1 = 1, C0 = 1 (WRITE DAC + EEPROM)
		TWI_Write_Data(hVout);
		TWI_Write_Data(lVout);

		TWI_Stop();//Stop to start Reading

		sw("s\n\r");
		while(DacRdy==0)
		{
			TWI_Start();//Start condition
			TWI_Write_Addr_Read(0B11000001);//Call the slave device for read.
			DacRdy = ReadDacReg(1,1);//Read if the eeprom is ready
		}
		sw("f\n\r");
		
	}
}

/*
Read all DAC output bytes (5 bytes) and print them on screen.
*/
uint8_t ReadDacReg (uint8_t readyOnly, uint8_t stop)
{
	//FIRST:
	//TWI_Start();//Start condition
	//TWI_Write_Addr_Read(0B11000001);//Call the slave device for read.

	if(readyOnly == 1)
	{	
		TWI_Write_Addr_Read_Repeatedly();//read the first byte of the IC

		uint8_t rdyBit; //Var to save the status (ready/not ready)
		rdyBit = BIT_GET(TWDR, BIT(7)); //get the status of the IC (if the eeprom done writing?)

		return rdyBit;//return the status (Ready=1, Not ready =0)

		if(stop == 1)
		{
			TWI_Stop();
		}
	}

	if(readyOnly==0)
	{
		TWI_Write_Addr_Read_Repeatedly();
		swn(TWDR, 2, 1);
		TWI_Write_Addr_Read_Repeatedly();
		swn(TWDR, 2, 1);
		TWI_Write_Addr_Read_Repeatedly();
		swn(TWDR, 2, 1);
		TWI_Write_Addr_Read_Repeatedly();
		swn(TWDR, 2, 1);
		TWI_Write_Addr_Read_Repeatedly();
		swn(TWDR, 2, 1);		
	}

	if(stop == 1)
	{
		TWI_Stop();
	}

	return -1; //return a default value
}

void init_rxtx_function()
{
	//==================================TX START====================================
	UBRR0H = (BRC >> 8); //Put BRC to UBRR0H and move it right 8 bits.
	UBRR0L = BRC;
	UCSR0B = (1 << TXEN0); //Trans enable
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); //8 BIT data frame

	//ENABLE interrupts
	sei();
	//==================================TX END=====================================
	//================================== RX START===========================================
	UCSR0B |= (1 << RXEN0) | (1 << RXCIE0); //RX enable and Receive complete interrupt enable
	//================================== RX END=============================================
}

void sw(const char toWrite[]){
	len = strlen(toWrite); //take size of characters to be printed
	k=0;	//initialize i
	UDR0=0; //make sure UDR0 is 0
	
	while (k<len) //while i is less than the total length of the characters to be printed
	{
		if (  ((UCSR0A & 0B00100000) == 0B00100000)  ){ //if UDRE0 is 1 (aka UDR0 is ready to send)
			UDR0 = toWrite[k]; //put the next character to be sent (now, UDRE0 is 0)
			k++;		//increase the position, and wait until UDRE0 is 1 again
		}
	}
	udr0 = '\0';
}

void swn(int num, int type, int ln) //take an int and print it on serial
{
	char str_intx[50];//declare a string of 50 characters
	
	itoa(num, str_intx, type);//convert from int to char and save it on str
	
	sw(str_intx); //serial write str
	
	if(ln == 1) //if we want a new line,
	{
		sw("\n\r");
	}
	
}

/*
Input: float number, output: print float number to serial, OR only return the float as string.
*/
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it) //Take a float and print it on serial
{
	/*
	Those do not work well
	*/
	/*if(extra_accur == 11)
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 1000)));
	}
	else if(extra_accur == 22)
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 100))); //edited to accept negative numbers
		//sprintf(str_floatx,"%d.%02u", (int) numf, (int) ((numf - (int) numf ) * 100) ); //this was te original function
	}*/
	
	if(extra_accur == 0 || extra_accur == 1)//works with very good accuracy
	{
		/*
		Needs library linking. Below tutorial is for Microchip studio.
		Project properties..XC8 Linker/General -> Tick 'Use vprintf library(-Wl,-u,vprintf)
		And: XC8 Linker/Miscallaneous insert at 'Other linker flags' this: -lprintf_flt
		https://startingelectronics.org/articles/atmel-AVR-8-bit/print-float-atmel-studio-7/
		*/
		sprintf(str_floatx, "%f", numf); //needs library linking or itll print something like ' ?); '
	}

	if(print_it)
	{
		sw(str_floatx);	
		
		if(lnf == 1) //if we want a new line,
		{
			sw("\n\r");
		}
	}
}

//==================================TX END=====================================

/*
the result (input) is stored in: rxBuffer
*/
void readUntill(char c_rx)
{
	rxWritePos = 0;//begin writing in the buffer from pos 0
	readString = 1;//interrupt will read and save strings
	rx_stop = c_rx; //interrupt will use the global var rx_stop to stop reading the string
	
	do{
		
	}while(readString == 1);
}

ISR(USART_RX_vect)
{
	if(readString == 1)
	{	
		rxBuffer[rxWritePos] = UDR0;
		if(rxBuffer[rxWritePos] == rx_stop)
		{
			readString = 0;
			/*when you initialize a character array by listing all of its characters 
			separately then you must supply the '\0' character explicitly */
			rxBuffer[rxWritePos] = '\0' ;
		}
		rxWritePos++;
	}
	else
	{
		udr0 = UDR0;	//udr0 variable can be used to store the input from the user.
	}
}

//==================================RX END==================================