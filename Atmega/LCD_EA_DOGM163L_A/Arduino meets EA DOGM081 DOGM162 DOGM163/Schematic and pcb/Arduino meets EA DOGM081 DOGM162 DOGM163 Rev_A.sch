<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="no" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="EA">
<packages>
<package name="EA_DOGM">
<description>&lt;b&gt;EA DOM081 / EA DOGM162 / EA DOGM163&lt;/b&gt;&lt;br&gt;
Textdisplays COG inkl.Kontroller&lt;br&gt;
Hersteller: ELECTRONIC ASSEMBLY GmbH&lt;br&gt;
http://www.lcd-module.de</description>
<wire x1="-3.3" y1="0.5" x2="51.5" y2="0.5" width="0.127" layer="21"/>
<wire x1="51.5" y1="0.5" x2="51.5" y2="27.5" width="0.127" layer="21"/>
<wire x1="51.5" y1="27.5" x2="-3.3" y2="27.5" width="0.127" layer="21"/>
<wire x1="-3.3" y1="27.5" x2="-3.3" y2="0.5" width="0.127" layer="21"/>
<wire x1="-1.4" y1="4.5" x2="49.6" y2="4.5" width="0.127" layer="21"/>
<wire x1="49.6" y1="4.5" x2="49.6" y2="19" width="0.127" layer="21"/>
<wire x1="49.6" y1="19" x2="-1.4" y2="19" width="0.127" layer="21"/>
<wire x1="-1.4" y1="19" x2="-1.4" y2="4.5" width="0.127" layer="21"/>
<wire x1="21.4" y1="23.8" x2="24.6" y2="23.8" width="0.127" layer="21"/>
<wire x1="24.6" y1="23.8" x2="24.6" y2="23.3" width="0.127" layer="21"/>
<wire x1="24.6" y1="23.3" x2="21.4" y2="23.3" width="0.127" layer="21"/>
<wire x1="21.4" y1="23.3" x2="21.4" y2="23.8" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="0.8128" shape="square"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="square"/>
<pad name="19" x="45.72" y="0" drill="0.8128" shape="square"/>
<pad name="20" x="48.26" y="0" drill="0.8128" shape="square"/>
<pad name="22" x="45.72" y="27.94" drill="0.8128" shape="square"/>
<pad name="21" x="48.26" y="27.94" drill="0.8128" shape="square"/>
<pad name="23" x="43.18" y="27.94" drill="0.8128" shape="square"/>
<pad name="24" x="40.64" y="27.94" drill="0.8128" shape="square"/>
<pad name="25" x="38.1" y="27.94" drill="0.8128" shape="square"/>
<pad name="26" x="35.56" y="27.94" drill="0.8128" shape="square"/>
<pad name="27" x="33.02" y="27.94" drill="0.8128" shape="square"/>
<pad name="28" x="30.48" y="27.94" drill="0.8128" shape="square"/>
<pad name="29" x="27.94" y="27.94" drill="0.8128" shape="square"/>
<pad name="30" x="25.4" y="27.94" drill="0.8128" shape="square"/>
<pad name="31" x="22.86" y="27.94" drill="0.8128" shape="square"/>
<pad name="32" x="20.32" y="27.94" drill="0.8128" shape="square"/>
<pad name="33" x="17.78" y="27.94" drill="0.8128" shape="square"/>
<pad name="34" x="15.24" y="27.94" drill="0.8128" shape="square"/>
<pad name="35" x="12.7" y="27.94" drill="0.8128" shape="square"/>
<pad name="36" x="10.16" y="27.94" drill="0.8128" shape="square"/>
<pad name="37" x="7.62" y="27.94" drill="0.8128" shape="square"/>
<pad name="38" x="5.08" y="27.94" drill="0.8128" shape="square"/>
<pad name="39" x="2.54" y="27.94" drill="0.8128" shape="square"/>
<pad name="40" x="0" y="27.94" drill="0.8128" shape="square"/>
<text x="-1.905" y="1.27" size="1.27" layer="21">1</text>
<text x="48.895" y="1.27" size="1.27" layer="21">20</text>
<text x="48.895" y="25.4" size="1.27" layer="21">21</text>
<text x="-2.54" y="25.4" size="1.27" layer="21">40</text>
<text x="18.415" y="20.32" size="1.27" layer="21">EA DOGM</text>
<text x="19.685" y="12.065" size="1.27" layer="25">&gt;name</text>
<text x="19.685" y="9.525" size="1.27" layer="27">&gt;value</text>
</package>
<package name="EA_LED55X31">
<wire x1="-3.3" y1="-1.5" x2="51.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="51.5" y1="-1.5" x2="51.5" y2="29.5" width="0.127" layer="21"/>
<wire x1="51.5" y1="29.5" x2="-3.3" y2="29.5" width="0.127" layer="21"/>
<wire x1="-3.3" y1="29.5" x2="-3.3" y2="-1.5" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="0.8128" shape="square"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="square"/>
<pad name="19" x="45.72" y="0" drill="0.8128" shape="square"/>
<pad name="20" x="48.26" y="0" drill="0.8128" shape="square"/>
<pad name="22" x="45.72" y="27.94" drill="0.8128" shape="square"/>
<pad name="21" x="48.26" y="27.94" drill="0.8128" shape="square"/>
<pad name="23" x="43.18" y="27.94" drill="0.8128" shape="square"/>
<pad name="24" x="40.64" y="27.94" drill="0.8128" shape="square"/>
<pad name="25" x="38.1" y="27.94" drill="0.8128" shape="square"/>
<pad name="26" x="35.56" y="27.94" drill="0.8128" shape="square"/>
<pad name="27" x="33.02" y="27.94" drill="0.8128" shape="square"/>
<pad name="28" x="30.48" y="27.94" drill="0.8128" shape="square"/>
<pad name="29" x="27.94" y="27.94" drill="0.8128" shape="square"/>
<pad name="30" x="25.4" y="27.94" drill="0.8128" shape="square"/>
<pad name="31" x="22.86" y="27.94" drill="0.8128" shape="square"/>
<pad name="32" x="20.32" y="27.94" drill="0.8128" shape="square"/>
<pad name="33" x="17.78" y="27.94" drill="0.8128" shape="square"/>
<pad name="34" x="15.24" y="27.94" drill="0.8128" shape="square"/>
<pad name="35" x="12.7" y="27.94" drill="0.8128" shape="square"/>
<pad name="36" x="10.16" y="27.94" drill="0.8128" shape="square"/>
<pad name="37" x="7.62" y="27.94" drill="0.8128" shape="square"/>
<pad name="38" x="5.08" y="27.94" drill="0.8128" shape="square"/>
<pad name="39" x="2.54" y="27.94" drill="0.8128" shape="square"/>
<pad name="40" x="0" y="27.94" drill="0.8128" shape="square"/>
<text x="-1.905" y="1.27" size="1.27" layer="21">1</text>
<text x="48.895" y="1.27" size="1.27" layer="21">20</text>
<text x="48.895" y="25.4" size="1.27" layer="21">21</text>
<text x="-2.54" y="25.4" size="1.27" layer="21">40</text>
<text x="18.415" y="17.78" size="1.27" layer="21">EA LED55x31</text>
<text x="19.685" y="6.985" size="1.27" layer="25">&gt;name</text>
<text x="19.685" y="4.445" size="1.27" layer="27">&gt;value</text>
</package>
<package name="R_10MM_AND_1206">
<wire x1="1.925" y1="-1.25" x2="8.155" y2="-1.25" width="0.254" layer="21"/>
<wire x1="8.155" y1="-1.25" x2="8.155" y2="1.25" width="0.254" layer="21"/>
<wire x1="8.155" y1="1.25" x2="1.925" y2="1.25" width="0.254" layer="21"/>
<wire x1="1.925" y1="1.25" x2="1.925" y2="-1.25" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="2" x="10.16" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<text x="0" y="-2.54" size="1.27" layer="27">&gt;value</text>
<text x="0" y="1.27" size="1.27" layer="25">&gt;name</text>
<smd name="3" x="6.5532" y="0" dx="1.143" dy="1.7018" layer="1"/>
<smd name="4" x="3.6068" y="0" dx="1.143" dy="1.7018" layer="1"/>
<wire x1="0" y1="0" x2="3.175" y2="0" width="0.2032" layer="1"/>
<wire x1="10.16" y1="0" x2="6.985" y2="0" width="0.2032" layer="1"/>
<wire x1="1.905" y1="0" x2="0.635" y2="0" width="0.3048" layer="21"/>
<wire x1="8.255" y1="0" x2="9.525" y2="0" width="0.3048" layer="21"/>
</package>
<package name="C_5MM_AND_1206">
<wire x1="1.747646875" y1="0" x2="2.178878125" y2="0" width="0.254" layer="21"/>
<wire x1="2.881125" y1="0" x2="3.332353125" y2="0" width="0.254" layer="21"/>
<wire x1="2.178878125" y1="0.77" x2="2.178878125" y2="0" width="0.254" layer="21"/>
<wire x1="2.178878125" y1="0" x2="2.178878125" y2="-0.77" width="0.254" layer="21"/>
<wire x1="2.881125" y1="-0.77" x2="2.881125" y2="0" width="0.254" layer="21"/>
<wire x1="2.881125" y1="0" x2="2.881125" y2="0.77" width="0.254" layer="21"/>
<wire x1="-1.25" y1="-1.25" x2="6.25" y2="-1.25" width="0.254" layer="21"/>
<wire x1="6.25" y1="-1.25" x2="6.25" y2="1.25" width="0.254" layer="21"/>
<wire x1="6.25" y1="1.25" x2="-1.25" y2="1.25" width="0.254" layer="21"/>
<wire x1="-1.25" y1="1.25" x2="-1.25" y2="-1.25" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<text x="0" y="-2.54" size="1.27" layer="27">&gt;value</text>
<text x="0" y="1.27" size="1.27" layer="25">&gt;name</text>
<smd name="3" x="4.0132" y="0" dx="1.143" dy="1.7018" layer="1"/>
<smd name="4" x="1.0668" y="0" dx="1.143" dy="1.7018" layer="1"/>
</package>
<package name="PAD55/35">
<pad name="1" x="0" y="0" drill="3.5" diameter="5.5"/>
<text x="-2" y="3.23" size="1.016" layer="25">&gt;name</text>
</package>
</packages>
<symbols>
<symbol name="EA_DOGM162">
<pin name="D0" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="D1" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="D2" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="D3" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="D4" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="D5" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="D6" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="D7" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="RS" x="15.24" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="R/!W!" x="15.24" y="-10.16" length="middle" direction="in" rot="R180"/>
<pin name="E" x="15.24" y="-12.7" length="middle" direction="in" rot="R180"/>
<pin name="!CSB!" x="15.24" y="-15.24" length="middle" direction="in" rot="R180"/>
<wire x1="10.16" y1="15.24" x2="-10.16" y2="15.24" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="10.16" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="10.16" y2="15.24" width="0.4064" layer="94"/>
<pin name="VDD" x="-15.24" y="12.7" length="middle" direction="pwr"/>
<pin name="GND" x="-15.24" y="10.16" length="middle" direction="pwr"/>
<pin name="CAP1N" x="-15.24" y="5.08" length="middle" direction="pas"/>
<pin name="CAP1P" x="-15.24" y="2.54" length="middle" direction="pas"/>
<pin name="VIN" x="-15.24" y="-2.54" length="middle" direction="pas"/>
<pin name="VOUT" x="-15.24" y="-5.08" length="middle" direction="pas"/>
<pin name="PSB" x="-15.24" y="-10.16" length="middle" direction="in"/>
<pin name="!RESET!" x="-15.24" y="-15.24" length="middle" direction="in"/>
<text x="-10.16" y="17.78" size="1.778" layer="94">&gt;name</text>
<text x="-10.16" y="15.24" size="1.778" layer="94">&gt;value</text>
</symbol>
<symbol name="EA_LED55X31_MONO">
<description>ELECTRONIC ASSEMBLY GmbH&lt;br&gt;
EA LED55x31-A&lt;br&gt;
EA LED55x31-B&lt;br&gt;
EA LED55x31-G&lt;br&gt;
EA LED55x31-R&lt;br&gt;
EA LED55x31-W</description>
<wire x1="-10.16" y1="2.54" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<pin name="C2" x="15.24" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="A2" x="15.24" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="A1" x="-15.24" y="0" length="middle" direction="pas"/>
<pin name="C1" x="-15.24" y="-2.54" length="middle" direction="pas"/>
<text x="-10.16" y="-7.62" size="1.778" layer="94">&gt;name</text>
<text x="-10.16" y="-10.16" size="1.778" layer="94">&gt;value</text>
</symbol>
<symbol name="R_10MM_AND_1206">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C_THT_AND_SMD">
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<text x="-3.556" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="-2.794" y1="-0.254" x2="1.27" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-3.81" y1="-0.254" x2="0.254" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TESTPUNKT">
<wire x1="0" y1="2.032" x2="2.032" y2="0" width="0.4064" layer="94"/>
<wire x1="2.032" y1="0" x2="0" y2="-2.032" width="0.4064" layer="94"/>
<wire x1="0" y1="-2.032" x2="-2.032" y2="0" width="0.4064" layer="94"/>
<wire x1="-2.032" y1="0" x2="0" y2="2.032" width="0.4064" layer="94"/>
<pin name="1" x="0" y="0" visible="pin" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EA_DOGM162" prefix="LCD">
<description>ELECTRONIC ASSEMBYL GmbH
EA DOGM162-A
EA DOGM081-A
EA DOGM163-A</description>
<gates>
<gate name="G$1" symbol="EA_DOGM162" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EA_DOGM">
<connects>
<connect gate="G$1" pin="!CSB!" pad="38"/>
<connect gate="G$1" pin="!RESET!" pad="40"/>
<connect gate="G$1" pin="CAP1N" pad="21"/>
<connect gate="G$1" pin="CAP1P" pad="22"/>
<connect gate="G$1" pin="D0" pad="35"/>
<connect gate="G$1" pin="D1" pad="34"/>
<connect gate="G$1" pin="D2" pad="33"/>
<connect gate="G$1" pin="D3" pad="32"/>
<connect gate="G$1" pin="D4" pad="31"/>
<connect gate="G$1" pin="D5" pad="30"/>
<connect gate="G$1" pin="D6" pad="29"/>
<connect gate="G$1" pin="D7" pad="28"/>
<connect gate="G$1" pin="E" pad="36"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="PSB" pad="23"/>
<connect gate="G$1" pin="R/!W!" pad="37"/>
<connect gate="G$1" pin="RS" pad="39"/>
<connect gate="G$1" pin="VDD" pad="26"/>
<connect gate="G$1" pin="VIN" pad="25"/>
<connect gate="G$1" pin="VOUT" pad="24"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EA_LED55X31_MONO" prefix="LED">
<gates>
<gate name="G$1" symbol="EA_LED55X31_MONO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EA_LED55X31">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="20"/>
<connect gate="G$1" pin="C1" pad="2"/>
<connect gate="G$1" pin="C2" pad="19"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R_10MM_AND_1206" prefix="R">
<gates>
<gate name="G$1" symbol="R_10MM_AND_1206" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R_10MM_AND_1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C_5MM_AND_1206" prefix="C">
<gates>
<gate name="G$1" symbol="C_THT_AND_SMD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C_5MM_AND_1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAD55/35">
<gates>
<gate name="G$1" symbol="TESTPUNKT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PAD55/35">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-npn">
<description>&lt;b&gt;NPN Transistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="TO92">
<description>&lt;b&gt;TO-92&lt;/b&gt;</description>
<wire x1="-2.0946" y1="-1.651" x2="-0.7863" y2="2.5485" width="0.127" layer="21" curve="-111.098957"/>
<wire x1="0.7868" y1="2.5484" x2="2.095" y2="-1.651" width="0.127" layer="21" curve="-111.09954"/>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.254" y1="-0.254" x2="-0.286" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-2.655" y1="-0.254" x2="-2.254" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.286" y1="-0.254" x2="0.286" y2="-0.254" width="0.127" layer="21"/>
<wire x1="2.254" y1="-0.254" x2="2.655" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.286" y1="-0.254" x2="2.254" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.7863" y1="2.5485" x2="0.7863" y2="2.5485" width="0.127" layer="51" curve="-34.293591"/>
<pad name="1" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="0" y="1.905" drill="0.8128" shape="octagon"/>
<pad name="3" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.635" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BC547" prefix="T">
<description>&lt;b&gt;NPN TRANSISTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO92">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="E14_Arduino">
<packages>
<package name="ARDUINO_UNO_SHIELD">
<hole x="66.04" y="7.62" drill="3.2"/>
<hole x="66.04" y="35.56" drill="3.2"/>
<hole x="15.24" y="50.8" drill="3.2"/>
<hole x="13.97" y="2.54" drill="3.2"/>
<wire x1="60.325" y1="52.07" x2="61.595" y2="52.07" width="0.2032" layer="21"/>
<wire x1="61.595" y1="52.07" x2="62.23" y2="51.435" width="0.2032" layer="21"/>
<wire x1="62.23" y1="50.165" x2="61.595" y2="49.53" width="0.2032" layer="21"/>
<wire x1="57.15" y1="51.435" x2="57.785" y2="52.07" width="0.2032" layer="21"/>
<wire x1="57.785" y1="52.07" x2="59.055" y2="52.07" width="0.2032" layer="21"/>
<wire x1="59.055" y1="52.07" x2="59.69" y2="51.435" width="0.2032" layer="21"/>
<wire x1="59.69" y1="50.165" x2="59.055" y2="49.53" width="0.2032" layer="21"/>
<wire x1="59.055" y1="49.53" x2="57.785" y2="49.53" width="0.2032" layer="21"/>
<wire x1="57.785" y1="49.53" x2="57.15" y2="50.165" width="0.2032" layer="21"/>
<wire x1="60.325" y1="52.07" x2="59.69" y2="51.435" width="0.2032" layer="21"/>
<wire x1="59.69" y1="50.165" x2="60.325" y2="49.53" width="0.2032" layer="21"/>
<wire x1="61.595" y1="49.53" x2="60.325" y2="49.53" width="0.2032" layer="21"/>
<wire x1="52.705" y1="52.07" x2="53.975" y2="52.07" width="0.2032" layer="21"/>
<wire x1="53.975" y1="52.07" x2="54.61" y2="51.435" width="0.2032" layer="21"/>
<wire x1="54.61" y1="50.165" x2="53.975" y2="49.53" width="0.2032" layer="21"/>
<wire x1="54.61" y1="51.435" x2="55.245" y2="52.07" width="0.2032" layer="21"/>
<wire x1="55.245" y1="52.07" x2="56.515" y2="52.07" width="0.2032" layer="21"/>
<wire x1="56.515" y1="52.07" x2="57.15" y2="51.435" width="0.2032" layer="21"/>
<wire x1="57.15" y1="50.165" x2="56.515" y2="49.53" width="0.2032" layer="21"/>
<wire x1="56.515" y1="49.53" x2="55.245" y2="49.53" width="0.2032" layer="21"/>
<wire x1="55.245" y1="49.53" x2="54.61" y2="50.165" width="0.2032" layer="21"/>
<wire x1="49.53" y1="51.435" x2="50.165" y2="52.07" width="0.2032" layer="21"/>
<wire x1="50.165" y1="52.07" x2="51.435" y2="52.07" width="0.2032" layer="21"/>
<wire x1="51.435" y1="52.07" x2="52.07" y2="51.435" width="0.2032" layer="21"/>
<wire x1="52.07" y1="50.165" x2="51.435" y2="49.53" width="0.2032" layer="21"/>
<wire x1="51.435" y1="49.53" x2="50.165" y2="49.53" width="0.2032" layer="21"/>
<wire x1="50.165" y1="49.53" x2="49.53" y2="50.165" width="0.2032" layer="21"/>
<wire x1="52.705" y1="52.07" x2="52.07" y2="51.435" width="0.2032" layer="21"/>
<wire x1="52.07" y1="50.165" x2="52.705" y2="49.53" width="0.2032" layer="21"/>
<wire x1="53.975" y1="49.53" x2="52.705" y2="49.53" width="0.2032" layer="21"/>
<wire x1="45.085" y1="52.07" x2="46.355" y2="52.07" width="0.2032" layer="21"/>
<wire x1="46.355" y1="52.07" x2="46.99" y2="51.435" width="0.2032" layer="21"/>
<wire x1="46.99" y1="50.165" x2="46.355" y2="49.53" width="0.2032" layer="21"/>
<wire x1="46.99" y1="51.435" x2="47.625" y2="52.07" width="0.2032" layer="21"/>
<wire x1="47.625" y1="52.07" x2="48.895" y2="52.07" width="0.2032" layer="21"/>
<wire x1="48.895" y1="52.07" x2="49.53" y2="51.435" width="0.2032" layer="21"/>
<wire x1="49.53" y1="50.165" x2="48.895" y2="49.53" width="0.2032" layer="21"/>
<wire x1="48.895" y1="49.53" x2="47.625" y2="49.53" width="0.2032" layer="21"/>
<wire x1="47.625" y1="49.53" x2="46.99" y2="50.165" width="0.2032" layer="21"/>
<wire x1="44.45" y1="51.435" x2="44.45" y2="50.165" width="0.2032" layer="21"/>
<wire x1="45.085" y1="52.07" x2="44.45" y2="51.435" width="0.2032" layer="21"/>
<wire x1="44.45" y1="50.165" x2="45.085" y2="49.53" width="0.2032" layer="21"/>
<wire x1="46.355" y1="49.53" x2="45.085" y2="49.53" width="0.2032" layer="21"/>
<wire x1="62.865" y1="52.07" x2="64.135" y2="52.07" width="0.2032" layer="21"/>
<wire x1="64.135" y1="52.07" x2="64.77" y2="51.435" width="0.2032" layer="21"/>
<wire x1="64.77" y1="51.435" x2="64.77" y2="50.165" width="0.2032" layer="21"/>
<wire x1="64.77" y1="50.165" x2="64.135" y2="49.53" width="0.2032" layer="21"/>
<wire x1="62.865" y1="52.07" x2="62.23" y2="51.435" width="0.2032" layer="21"/>
<wire x1="62.23" y1="50.165" x2="62.865" y2="49.53" width="0.2032" layer="21"/>
<wire x1="64.135" y1="49.53" x2="62.865" y2="49.53" width="0.2032" layer="21"/>
<wire x1="38.481" y1="52.07" x2="39.751" y2="52.07" width="0.2032" layer="21"/>
<wire x1="39.751" y1="52.07" x2="40.386" y2="51.435" width="0.2032" layer="21"/>
<wire x1="40.386" y1="50.165" x2="39.751" y2="49.53" width="0.2032" layer="21"/>
<wire x1="35.306" y1="51.435" x2="35.941" y2="52.07" width="0.2032" layer="21"/>
<wire x1="35.941" y1="52.07" x2="37.211" y2="52.07" width="0.2032" layer="21"/>
<wire x1="37.211" y1="52.07" x2="37.846" y2="51.435" width="0.2032" layer="21"/>
<wire x1="37.846" y1="50.165" x2="37.211" y2="49.53" width="0.2032" layer="21"/>
<wire x1="37.211" y1="49.53" x2="35.941" y2="49.53" width="0.2032" layer="21"/>
<wire x1="35.941" y1="49.53" x2="35.306" y2="50.165" width="0.2032" layer="21"/>
<wire x1="38.481" y1="52.07" x2="37.846" y2="51.435" width="0.2032" layer="21"/>
<wire x1="37.846" y1="50.165" x2="38.481" y2="49.53" width="0.2032" layer="21"/>
<wire x1="39.751" y1="49.53" x2="38.481" y2="49.53" width="0.2032" layer="21"/>
<wire x1="30.861" y1="52.07" x2="32.131" y2="52.07" width="0.2032" layer="21"/>
<wire x1="32.131" y1="52.07" x2="32.766" y2="51.435" width="0.2032" layer="21"/>
<wire x1="32.766" y1="50.165" x2="32.131" y2="49.53" width="0.2032" layer="21"/>
<wire x1="32.766" y1="51.435" x2="33.401" y2="52.07" width="0.2032" layer="21"/>
<wire x1="33.401" y1="52.07" x2="34.671" y2="52.07" width="0.2032" layer="21"/>
<wire x1="34.671" y1="52.07" x2="35.306" y2="51.435" width="0.2032" layer="21"/>
<wire x1="35.306" y1="50.165" x2="34.671" y2="49.53" width="0.2032" layer="21"/>
<wire x1="34.671" y1="49.53" x2="33.401" y2="49.53" width="0.2032" layer="21"/>
<wire x1="33.401" y1="49.53" x2="32.766" y2="50.165" width="0.2032" layer="21"/>
<wire x1="27.686" y1="51.435" x2="28.321" y2="52.07" width="0.2032" layer="21"/>
<wire x1="28.321" y1="52.07" x2="29.591" y2="52.07" width="0.2032" layer="21"/>
<wire x1="29.591" y1="52.07" x2="30.226" y2="51.435" width="0.2032" layer="21"/>
<wire x1="30.226" y1="50.165" x2="29.591" y2="49.53" width="0.2032" layer="21"/>
<wire x1="29.591" y1="49.53" x2="28.321" y2="49.53" width="0.2032" layer="21"/>
<wire x1="28.321" y1="49.53" x2="27.686" y2="50.165" width="0.2032" layer="21"/>
<wire x1="30.861" y1="52.07" x2="30.226" y2="51.435" width="0.2032" layer="21"/>
<wire x1="30.226" y1="50.165" x2="30.861" y2="49.53" width="0.2032" layer="21"/>
<wire x1="32.131" y1="49.53" x2="30.861" y2="49.53" width="0.2032" layer="21"/>
<wire x1="23.241" y1="52.07" x2="24.511" y2="52.07" width="0.2032" layer="21"/>
<wire x1="24.511" y1="52.07" x2="25.146" y2="51.435" width="0.2032" layer="21"/>
<wire x1="25.146" y1="50.165" x2="24.511" y2="49.53" width="0.2032" layer="21"/>
<wire x1="25.146" y1="51.435" x2="25.781" y2="52.07" width="0.2032" layer="21"/>
<wire x1="25.781" y1="52.07" x2="27.051" y2="52.07" width="0.2032" layer="21"/>
<wire x1="27.051" y1="52.07" x2="27.686" y2="51.435" width="0.2032" layer="21"/>
<wire x1="27.686" y1="50.165" x2="27.051" y2="49.53" width="0.2032" layer="21"/>
<wire x1="27.051" y1="49.53" x2="25.781" y2="49.53" width="0.2032" layer="21"/>
<wire x1="25.781" y1="49.53" x2="25.146" y2="50.165" width="0.2032" layer="21"/>
<wire x1="23.241" y1="52.07" x2="22.606" y2="51.435" width="0.2032" layer="21"/>
<wire x1="22.606" y1="50.165" x2="23.241" y2="49.53" width="0.2032" layer="21"/>
<wire x1="24.511" y1="49.53" x2="23.241" y2="49.53" width="0.2032" layer="21"/>
<wire x1="41.021" y1="52.07" x2="42.291" y2="52.07" width="0.2032" layer="21"/>
<wire x1="42.291" y1="52.07" x2="42.926" y2="51.435" width="0.2032" layer="21"/>
<wire x1="42.926" y1="51.435" x2="42.926" y2="50.165" width="0.2032" layer="21"/>
<wire x1="42.926" y1="50.165" x2="42.291" y2="49.53" width="0.2032" layer="21"/>
<wire x1="41.021" y1="52.07" x2="40.386" y2="51.435" width="0.2032" layer="21"/>
<wire x1="40.386" y1="50.165" x2="41.021" y2="49.53" width="0.2032" layer="21"/>
<wire x1="42.291" y1="49.53" x2="41.021" y2="49.53" width="0.2032" layer="21"/>
<pad name="6" x="48.26" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="50.8" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="53.34" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="55.88" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="58.42" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="1" x="60.96" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="0" x="63.5" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="45.72" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="GND" x="26.416" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="13" x="28.956" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="12" x="31.496" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="34.036" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="10" x="36.576" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="39.116" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="41.656" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="AREF" x="23.876" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="25.74925" y="52.48275" size="0.6096" layer="21" font="vector" ratio="15">GND</text>
<text x="23.0505" y="52.451" size="0.6096" layer="21" font="vector" ratio="15">AREF</text>
<wire x1="20.701" y1="52.07" x2="21.971" y2="52.07" width="0.2032" layer="21"/>
<wire x1="21.971" y1="52.07" x2="22.606" y2="51.435" width="0.2032" layer="21"/>
<wire x1="22.606" y1="50.165" x2="21.971" y2="49.53" width="0.2032" layer="21"/>
<wire x1="20.701" y1="52.07" x2="20.066" y2="51.435" width="0.2032" layer="21"/>
<wire x1="20.066" y1="50.165" x2="20.701" y2="49.53" width="0.2032" layer="21"/>
<wire x1="21.971" y1="49.53" x2="20.701" y2="49.53" width="0.2032" layer="21"/>
<pad name="SDA" x="21.336" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<wire x1="18.161" y1="52.07" x2="19.431" y2="52.07" width="0.2032" layer="21"/>
<wire x1="19.431" y1="52.07" x2="20.066" y2="51.435" width="0.2032" layer="21"/>
<wire x1="20.066" y1="50.165" x2="19.431" y2="49.53" width="0.2032" layer="21"/>
<wire x1="17.526" y1="51.435" x2="17.526" y2="50.165" width="0.2032" layer="21"/>
<wire x1="18.161" y1="52.07" x2="17.526" y2="51.435" width="0.2032" layer="21"/>
<wire x1="17.526" y1="50.165" x2="18.161" y2="49.53" width="0.2032" layer="21"/>
<wire x1="19.431" y1="49.53" x2="18.161" y2="49.53" width="0.2032" layer="21"/>
<pad name="SCL" x="18.796" y="50.8" drill="1.016" diameter="1.8796" rot="R90"/>
<wire x1="62.23" y1="3.175" x2="62.865" y2="3.81" width="0.2032" layer="21"/>
<wire x1="62.865" y1="3.81" x2="64.135" y2="3.81" width="0.2032" layer="21"/>
<wire x1="64.135" y1="3.81" x2="64.77" y2="3.175" width="0.2032" layer="21"/>
<wire x1="64.77" y1="1.905" x2="64.135" y2="1.27" width="0.2032" layer="21"/>
<wire x1="64.135" y1="1.27" x2="62.865" y2="1.27" width="0.2032" layer="21"/>
<wire x1="62.865" y1="1.27" x2="62.23" y2="1.905" width="0.2032" layer="21"/>
<wire x1="57.785" y1="3.81" x2="59.055" y2="3.81" width="0.2032" layer="21"/>
<wire x1="59.055" y1="3.81" x2="59.69" y2="3.175" width="0.2032" layer="21"/>
<wire x1="59.69" y1="1.905" x2="59.055" y2="1.27" width="0.2032" layer="21"/>
<wire x1="59.69" y1="3.175" x2="60.325" y2="3.81" width="0.2032" layer="21"/>
<wire x1="60.325" y1="3.81" x2="61.595" y2="3.81" width="0.2032" layer="21"/>
<wire x1="61.595" y1="3.81" x2="62.23" y2="3.175" width="0.2032" layer="21"/>
<wire x1="62.23" y1="1.905" x2="61.595" y2="1.27" width="0.2032" layer="21"/>
<wire x1="61.595" y1="1.27" x2="60.325" y2="1.27" width="0.2032" layer="21"/>
<wire x1="60.325" y1="1.27" x2="59.69" y2="1.905" width="0.2032" layer="21"/>
<wire x1="54.61" y1="3.175" x2="55.245" y2="3.81" width="0.2032" layer="21"/>
<wire x1="55.245" y1="3.81" x2="56.515" y2="3.81" width="0.2032" layer="21"/>
<wire x1="56.515" y1="3.81" x2="57.15" y2="3.175" width="0.2032" layer="21"/>
<wire x1="57.15" y1="1.905" x2="56.515" y2="1.27" width="0.2032" layer="21"/>
<wire x1="56.515" y1="1.27" x2="55.245" y2="1.27" width="0.2032" layer="21"/>
<wire x1="55.245" y1="1.27" x2="54.61" y2="1.905" width="0.2032" layer="21"/>
<wire x1="57.785" y1="3.81" x2="57.15" y2="3.175" width="0.2032" layer="21"/>
<wire x1="57.15" y1="1.905" x2="57.785" y2="1.27" width="0.2032" layer="21"/>
<wire x1="59.055" y1="1.27" x2="57.785" y2="1.27" width="0.2032" layer="21"/>
<wire x1="50.165" y1="3.81" x2="51.435" y2="3.81" width="0.2032" layer="21"/>
<wire x1="51.435" y1="3.81" x2="52.07" y2="3.175" width="0.2032" layer="21"/>
<wire x1="52.07" y1="1.905" x2="51.435" y2="1.27" width="0.2032" layer="21"/>
<wire x1="52.07" y1="3.175" x2="52.705" y2="3.81" width="0.2032" layer="21"/>
<wire x1="52.705" y1="3.81" x2="53.975" y2="3.81" width="0.2032" layer="21"/>
<wire x1="53.975" y1="3.81" x2="54.61" y2="3.175" width="0.2032" layer="21"/>
<wire x1="54.61" y1="1.905" x2="53.975" y2="1.27" width="0.2032" layer="21"/>
<wire x1="53.975" y1="1.27" x2="52.705" y2="1.27" width="0.2032" layer="21"/>
<wire x1="52.705" y1="1.27" x2="52.07" y2="1.905" width="0.2032" layer="21"/>
<wire x1="49.53" y1="3.175" x2="49.53" y2="1.905" width="0.2032" layer="21"/>
<wire x1="50.165" y1="3.81" x2="49.53" y2="3.175" width="0.2032" layer="21"/>
<wire x1="49.53" y1="1.905" x2="50.165" y2="1.27" width="0.2032" layer="21"/>
<wire x1="51.435" y1="1.27" x2="50.165" y2="1.27" width="0.2032" layer="21"/>
<wire x1="44.45" y1="3.175" x2="45.085" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="46.355" y1="3.81" x2="46.99" y2="3.175" width="0.2032" layer="21"/>
<wire x1="46.99" y1="1.905" x2="46.355" y2="1.27" width="0.2032" layer="21"/>
<wire x1="46.355" y1="1.27" x2="45.085" y2="1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="1.27" x2="44.45" y2="1.905" width="0.2032" layer="21"/>
<wire x1="40.005" y1="3.81" x2="41.275" y2="3.81" width="0.2032" layer="21"/>
<wire x1="41.275" y1="3.81" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="1.905" x2="41.275" y2="1.27" width="0.2032" layer="21"/>
<wire x1="41.91" y1="3.175" x2="42.545" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="43.815" y1="3.81" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="43.815" y2="1.27" width="0.2032" layer="21"/>
<wire x1="43.815" y1="1.27" x2="42.545" y2="1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.91" y2="1.905" width="0.2032" layer="21"/>
<wire x1="36.83" y1="3.175" x2="37.465" y2="3.81" width="0.2032" layer="21"/>
<wire x1="37.465" y1="3.81" x2="38.735" y2="3.81" width="0.2032" layer="21"/>
<wire x1="38.735" y1="3.81" x2="39.37" y2="3.175" width="0.2032" layer="21"/>
<wire x1="39.37" y1="1.905" x2="38.735" y2="1.27" width="0.2032" layer="21"/>
<wire x1="38.735" y1="1.27" x2="37.465" y2="1.27" width="0.2032" layer="21"/>
<wire x1="37.465" y1="1.27" x2="36.83" y2="1.905" width="0.2032" layer="21"/>
<wire x1="40.005" y1="3.81" x2="39.37" y2="3.175" width="0.2032" layer="21"/>
<wire x1="39.37" y1="1.905" x2="40.005" y2="1.27" width="0.2032" layer="21"/>
<wire x1="41.275" y1="1.27" x2="40.005" y2="1.27" width="0.2032" layer="21"/>
<wire x1="32.385" y1="3.81" x2="33.655" y2="3.81" width="0.2032" layer="21"/>
<wire x1="33.655" y1="3.81" x2="34.29" y2="3.175" width="0.2032" layer="21"/>
<wire x1="34.29" y1="1.905" x2="33.655" y2="1.27" width="0.2032" layer="21"/>
<wire x1="34.29" y1="3.175" x2="34.925" y2="3.81" width="0.2032" layer="21"/>
<wire x1="34.925" y1="3.81" x2="36.195" y2="3.81" width="0.2032" layer="21"/>
<wire x1="36.195" y1="3.81" x2="36.83" y2="3.175" width="0.2032" layer="21"/>
<wire x1="36.83" y1="1.905" x2="36.195" y2="1.27" width="0.2032" layer="21"/>
<wire x1="36.195" y1="1.27" x2="34.925" y2="1.27" width="0.2032" layer="21"/>
<wire x1="34.925" y1="1.27" x2="34.29" y2="1.905" width="0.2032" layer="21"/>
<wire x1="32.385" y1="3.81" x2="31.75" y2="3.175" width="0.2032" layer="21"/>
<wire x1="31.75" y1="1.905" x2="32.385" y2="1.27" width="0.2032" layer="21"/>
<wire x1="33.655" y1="1.27" x2="32.385" y2="1.27" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="1.905" width="0.2032" layer="21"/>
<pad name="A1" x="53.34" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="A2" x="55.88" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="A3" x="58.42" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="A4" x="60.96" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="A5" x="63.5" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="A0" x="50.8" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="RESET" x="33.02" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3.3V" x="35.56" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5V" x="38.1" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="GND.." x="40.64" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="GND." x="43.18" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="VIN" x="45.72" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="51.2826" y="0.92075" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">A0</text>
<text x="53.8226" y="0.98425" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">A1</text>
<text x="56.52135" y="0.92075" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">A2</text>
<text x="59.06135" y="0.92075" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">A3</text>
<text x="61.60135" y="0.92075" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">A4</text>
<text x="64.14135" y="0.9525" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">A5</text>
<text x="34.29635" y="0.98425" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">RESET</text>
<text x="36.6776" y="0.98425" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">3.3V</text>
<text x="38.74135" y="0.98425" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">5V</text>
<text x="41.4401" y="0.98425" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">GND</text>
<text x="43.9801" y="0.98425" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">GND</text>
<text x="46.5201" y="0.98425" size="0.6096" layer="21" font="vector" ratio="15" rot="R180">Vin</text>
<wire x1="29.845" y1="3.81" x2="31.115" y2="3.81" width="0.2032" layer="21"/>
<wire x1="31.115" y1="3.81" x2="31.75" y2="3.175" width="0.2032" layer="21"/>
<wire x1="31.75" y1="1.905" x2="31.115" y2="1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="3.81" x2="29.21" y2="3.175" width="0.2032" layer="21"/>
<wire x1="29.21" y1="1.905" x2="29.845" y2="1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="1.27" x2="29.845" y2="1.27" width="0.2032" layer="21"/>
<pad name="I/OREF" x="30.48" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<wire x1="27.305" y1="3.81" x2="28.575" y2="3.81" width="0.2032" layer="21"/>
<wire x1="28.575" y1="3.81" x2="29.21" y2="3.175" width="0.2032" layer="21"/>
<wire x1="29.21" y1="1.905" x2="28.575" y2="1.27" width="0.2032" layer="21"/>
<wire x1="26.67" y1="3.175" x2="26.67" y2="1.905" width="0.2032" layer="21"/>
<wire x1="27.305" y1="3.81" x2="26.67" y2="3.175" width="0.2032" layer="21"/>
<wire x1="26.67" y1="1.905" x2="27.305" y2="1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="27.305" y2="1.27" width="0.2032" layer="21"/>
<pad name="N/C" x="27.94" y="2.54" drill="1.016" diameter="1.8796" rot="R90"/>
<wire x1="-6.35" y1="32.385" x2="0" y2="32.385" width="0.127" layer="21" style="shortdash"/>
<wire x1="0" y1="43.815" x2="-6.35" y2="43.815" width="0.127" layer="21" style="shortdash"/>
<wire x1="-6.35" y1="43.815" x2="-6.35" y2="32.385" width="0.127" layer="21" style="shortdash"/>
<wire x1="-1.778" y1="3.2766" x2="-0.3302" y2="3.2766" width="0.127" layer="21" style="shortdash"/>
<wire x1="-0.3302" y1="12.2174" x2="-1.778" y2="12.2174" width="0.127" layer="21" style="shortdash"/>
<wire x1="-1.778" y1="12.2174" x2="-1.778" y2="3.2766" width="0.127" layer="21" style="shortdash"/>
<wire x1="64.77" y1="3.175" x2="64.77" y2="1.905" width="0.2032" layer="21"/>
<text x="63.1825" y="52.3875" size="0.6096" layer="21" font="vector" ratio="15">0</text>
<text x="60.6425" y="52.3875" size="0.6096" layer="21" font="vector" ratio="15">1</text>
<text x="58.26125" y="52.3875" size="0.6096" layer="21" font="vector" ratio="15">2</text>
<text x="55.5625" y="52.41925" size="0.6096" layer="21" font="vector" ratio="15">3</text>
<text x="53.0225" y="52.3875" size="0.6096" layer="21" font="vector" ratio="15">4</text>
<text x="50.64125" y="52.3875" size="0.6096" layer="21" font="vector" ratio="15">5</text>
<text x="47.9425" y="52.5145" size="0.6096" layer="21" font="vector" ratio="15">6</text>
<text x="45.43425" y="52.5145" size="0.6096" layer="21" font="vector" ratio="15">7</text>
<text x="41.30675" y="52.451" size="0.6096" layer="21" font="vector" ratio="15">8</text>
<text x="38.9255" y="52.451" size="0.6096" layer="21" font="vector" ratio="15">9</text>
<text x="36.068" y="52.451" size="0.6096" layer="21" font="vector" ratio="15">10</text>
<text x="33.36925" y="52.48275" size="0.6096" layer="21" font="vector" ratio="15">11</text>
<text x="30.82925" y="52.48275" size="0.6096" layer="21" font="vector" ratio="15">12</text>
<text x="28.28925" y="52.48275" size="0.6096" layer="21" font="vector" ratio="15">13</text>
<text x="18.2499" y="52.3875" size="0.6096" layer="21" font="vector" ratio="15">A5</text>
<text x="20.94865" y="52.41925" size="0.6096" layer="21" font="vector" ratio="15">A4</text>
</package>
</packages>
<symbols>
<symbol name="ARDUINO_UNO_SHIELD">
<description>Arduino Shield</description>
<pin name="A5(SCL)." x="-22.86" y="20.32" length="short" rot="R270"/>
<pin name="A4(SDA)." x="-20.32" y="20.32" length="short" rot="R270"/>
<pin name="AREF" x="-17.78" y="20.32" length="short" rot="R270"/>
<pin name="GND" x="-15.24" y="20.32" length="short" rot="R270"/>
<pin name="13" x="-12.7" y="20.32" length="short" rot="R270"/>
<pin name="12" x="-10.16" y="20.32" length="short" rot="R270"/>
<pin name="~11" x="-7.62" y="20.32" length="short" rot="R270"/>
<pin name="~10" x="-5.08" y="20.32" length="short" rot="R270"/>
<pin name="~9" x="-2.54" y="20.32" length="short" rot="R270"/>
<pin name="8" x="0" y="20.32" length="short" rot="R270"/>
<pin name="7" x="7.62" y="20.32" length="short" rot="R270"/>
<pin name="~6" x="10.16" y="20.32" length="short" rot="R270"/>
<pin name="~5" x="12.7" y="20.32" length="short" rot="R270"/>
<pin name="4" x="15.24" y="20.32" length="short" rot="R270"/>
<pin name="~3" x="17.78" y="20.32" length="short" rot="R270"/>
<pin name="2" x="20.32" y="20.32" length="short" rot="R270"/>
<pin name="1(TX)" x="22.86" y="20.32" length="short" rot="R270"/>
<pin name="0(RX)" x="25.4" y="20.32" length="short" rot="R270"/>
<pin name="A5(SCL)" x="25.4" y="-22.86" length="short" rot="R90"/>
<pin name="A4(SDA)" x="22.86" y="-22.86" length="short" rot="R90"/>
<pin name="A3" x="20.32" y="-22.86" length="short" rot="R90"/>
<pin name="A2" x="17.78" y="-22.86" length="short" rot="R90"/>
<pin name="A1" x="15.24" y="-22.86" length="short" rot="R90"/>
<pin name="A0" x="12.7" y="-22.86" length="short" rot="R90"/>
<pin name="VIN" x="5.08" y="-22.86" length="short" rot="R90"/>
<pin name="GND." x="2.54" y="-22.86" length="short" rot="R90"/>
<pin name="GND.." x="0" y="-22.86" length="short" rot="R90"/>
<pin name="5V" x="-2.54" y="-22.86" length="short" rot="R90"/>
<pin name="3.3V" x="-5.08" y="-22.86" length="short" rot="R90"/>
<pin name="RESET" x="-7.62" y="-22.86" length="short" rot="R90"/>
<pin name="IOREF" x="-10.16" y="-22.86" length="short" rot="R90"/>
<pin name="(N/C)" x="-12.7" y="-22.86" length="short" rot="R90"/>
<wire x1="-30.48" y1="17.78" x2="27.94" y2="17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="17.78" x2="27.94" y2="10.16" width="0.254" layer="94"/>
<wire x1="27.94" y1="10.16" x2="30.48" y2="7.62" width="0.254" layer="94"/>
<wire x1="30.48" y1="7.62" x2="30.48" y2="-17.78" width="0.254" layer="94"/>
<wire x1="30.48" y1="-17.78" x2="27.94" y2="-20.32" width="0.254" layer="94"/>
<wire x1="27.94" y1="-20.32" x2="-30.48" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-20.32" x2="-30.48" y2="17.78" width="0.254" layer="94"/>
<text x="-17.78" y="-2.54" size="3.81" layer="94">ARDUINO UNO</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARDUINO_UNO_SHIELD" prefix="SCH">
<gates>
<gate name="G$1" symbol="ARDUINO_UNO_SHIELD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ARDUINO_UNO_SHIELD">
<connects>
<connect gate="G$1" pin="(N/C)" pad="N/C"/>
<connect gate="G$1" pin="0(RX)" pad="0"/>
<connect gate="G$1" pin="1(TX)" pad="1"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3.3V" pad="3.3V"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="A0" pad="A0"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4(SDA)" pad="A4"/>
<connect gate="G$1" pin="A4(SDA)." pad="SDA"/>
<connect gate="G$1" pin="A5(SCL)" pad="A5"/>
<connect gate="G$1" pin="A5(SCL)." pad="SCL"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND." pad="GND."/>
<connect gate="G$1" pin="GND.." pad="GND.."/>
<connect gate="G$1" pin="IOREF" pad="I/OREF"/>
<connect gate="G$1" pin="RESET" pad="RESET"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
<connect gate="G$1" pin="~10" pad="10"/>
<connect gate="G$1" pin="~11" pad="11"/>
<connect gate="G$1" pin="~3" pad="3"/>
<connect gate="G$1" pin="~5" pad="5"/>
<connect gate="G$1" pin="~6" pad="6"/>
<connect gate="G$1" pin="~9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames and Ports for Schematic and Layout&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
<symbol name="DINA4_L">
<wire x1="264.16" y1="0" x2="264.16" y2="180.34" width="0.4064" layer="94"/>
<wire x1="264.16" y1="180.34" x2="0" y2="180.34" width="0.4064" layer="94"/>
<wire x1="0" y1="180.34" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="0" x2="264.16" y2="0" width="0.4064" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="F">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
<variantdef name="3.3V monochrome"/>
<variantdef name="5V monochrome" current="yes"/>
<variantdef name="3.3V RGB"/>
<variantdef name="5V RGB"/>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="PCB1" library="E14_Arduino" deviceset="ARDUINO_UNO_SHIELD" device=""/>
<part name="LCD1" library="EA" deviceset="EA_DOGM162" device="" value="EA DOGMxxxx-A"/>
<part name="LED1" library="EA" deviceset="EA_LED55X31_MONO" device="" value="EA LED55X31-xxx"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="+5V" library="supply1" deviceset="+5V" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="R1" library="EA" deviceset="R_10MM_AND_1206" device="" value="0R">
<variant name="5V monochrome" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="C3" library="EA" deviceset="C_5MM_AND_1206" device="" value="1uF">
<variant name="5V monochrome" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="C1" library="EA" deviceset="C_5MM_AND_1206" device="" value="1uF">
<variant name="5V monochrome" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="R2" library="EA" deviceset="R_10MM_AND_1206" device="" value="0R">
<variant name="3.3V monochrome" populate="no"/>
<variant name="3.3V RGB" populate="no"/>
</part>
<part name="R3" library="EA" deviceset="R_10MM_AND_1206" device="" value="2k7"/>
<part name="R4" library="EA" deviceset="R_10MM_AND_1206" device="" value="4k7">
<variant name="5V monochrome" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="R5" library="EA" deviceset="R_10MM_AND_1206" device="" value="2k7"/>
<part name="R6" library="EA" deviceset="R_10MM_AND_1206" device="" value="4k7">
<variant name="5V monochrome" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="R7" library="EA" deviceset="R_10MM_AND_1206" device="" value="2k7"/>
<part name="R8" library="EA" deviceset="R_10MM_AND_1206" device="" value="4k7">
<variant name="5V monochrome" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="R9" library="EA" deviceset="R_10MM_AND_1206" device="" value="2k7"/>
<part name="R10" library="EA" deviceset="R_10MM_AND_1206" device="" value="4k7">
<variant name="5V monochrome" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="R11" library="EA" deviceset="R_10MM_AND_1206" device="" value="2k7"/>
<part name="R12" library="EA" deviceset="R_10MM_AND_1206" device="" value="4k7">
<variant name="5V monochrome" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="R25" library="EA" deviceset="R_10MM_AND_1206" device="" value="470R"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
<part name="T2" library="transistor-npn" deviceset="BC547" device="">
<variant name="3.3V monochrome" populate="no"/>
<variant name="5V monochrome" populate="no"/>
</part>
<part name="T1" library="transistor-npn" deviceset="BC547" device=""/>
<part name="T3" library="transistor-npn" deviceset="BC547" device=""/>
<part name="R20" library="EA" deviceset="R_10MM_AND_1206" device=""/>
<part name="R21" library="EA" deviceset="R_10MM_AND_1206" device="">
<variant name="3.3V monochrome" populate="no"/>
<variant name="5V monochrome" populate="no"/>
</part>
<part name="R22" library="EA" deviceset="R_10MM_AND_1206" device=""/>
<part name="R23" library="EA" deviceset="R_10MM_AND_1206" device="" value="0R">
<variant name="3.3V RGB" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="R26" library="EA" deviceset="R_10MM_AND_1206" device="" value="470R">
<variant name="3.3V monochrome" populate="no"/>
<variant name="5V monochrome" populate="no"/>
</part>
<part name="R27" library="EA" deviceset="R_10MM_AND_1206" device="" value="470R">
<variant name="3.3V monochrome" populate="no"/>
<variant name="5V monochrome" populate="no"/>
</part>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="F2" library="frames" deviceset="DINA4_L" device=""/>
<part name="R24" library="EA" deviceset="R_10MM_AND_1206" device="" value="470R">
<variant name="3.3V RGB" populate="no"/>
<variant name="5V RGB" populate="no"/>
</part>
<part name="GND" library="EA" deviceset="PAD55/35" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="189.23" y="27.94" size="2.54" layer="94">Arduino Uno Shield for</text>
<text x="167.64" y="24.13" size="2.54" layer="94">EA DOGM081-A, EA DOGM162-A, EA DOGM163-A</text>
<text x="256.54" y="7.62" size="2.54" layer="94">A</text>
<text x="185.42" y="31.75" size="2.54" layer="94">ELECTRONIC ASSEMBLY GmbH</text>
<text x="203.2" y="7.62" size="2.54" layer="94">EA PCBARDDOG7036</text>
</plain>
<instances>
<instance part="PCB1" gate="G$1" x="210.82" y="110.49" rot="R270"/>
<instance part="LCD1" gate="G$1" x="52.07" y="144.78"/>
<instance part="LED1" gate="G$1" x="52.07" y="124.46"/>
<instance part="GND1" gate="1" x="31.75" y="154.94" rot="R270"/>
<instance part="GND2" gate="1" x="31.75" y="134.62" rot="R270"/>
<instance part="GND3" gate="1" x="149.86" y="151.13"/>
<instance part="GND7" gate="1" x="78.74" y="134.62" rot="R90"/>
<instance part="GND8" gate="1" x="181.61" y="110.49" rot="R270"/>
<instance part="GND9" gate="1" x="237.49" y="125.73" rot="R90"/>
<instance part="+3V1" gate="G$1" x="88.9" y="165.1" smashed="yes" rot="R270">
<attribute name="VALUE" x="90.17" y="165.1" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="1" x="88.9" y="160.02" smashed="yes" rot="R270">
<attribute name="VALUE" x="90.17" y="160.02" size="1.778" layer="96"/>
</instance>
<instance part="+5V" gate="1" x="173.99" y="113.03" smashed="yes" rot="R90">
<attribute name="VALUE" x="172.72" y="113.03" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+3V2" gate="G$1" x="168.91" y="115.57" smashed="yes" rot="R90">
<attribute name="VALUE" x="167.64" y="116.84" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND4" gate="1" x="149.86" y="121.92"/>
<instance part="GND6" gate="1" x="149.86" y="92.71"/>
<instance part="GND12" gate="1" x="149.86" y="63.5"/>
<instance part="GND14" gate="1" x="149.86" y="34.29"/>
<instance part="R1" gate="G$1" x="80.01" y="165.1"/>
<instance part="C3" gate="G$1" x="20.32" y="153.67" rot="R90"/>
<instance part="C1" gate="G$1" x="15.3162" y="138.43" rot="R90"/>
<instance part="R2" gate="G$1" x="80.01" y="160.02"/>
<instance part="R3" gate="G$1" x="139.7" y="166.37"/>
<instance part="R4" gate="G$1" x="149.86" y="160.02" rot="R90"/>
<instance part="R5" gate="G$1" x="139.7" y="137.16"/>
<instance part="R6" gate="G$1" x="149.86" y="130.8354" rot="R90"/>
<instance part="R7" gate="G$1" x="139.7" y="107.95"/>
<instance part="R8" gate="G$1" x="149.86" y="101.6" rot="R90"/>
<instance part="R9" gate="G$1" x="139.7" y="78.74"/>
<instance part="R10" gate="G$1" x="149.86" y="72.39" rot="R90"/>
<instance part="R11" gate="G$1" x="139.7" y="49.53"/>
<instance part="R12" gate="G$1" x="149.86" y="43.18" rot="R90"/>
<instance part="R25" gate="G$1" x="63.5" y="96.52"/>
<instance part="GND5" gate="1" x="93.98" y="67.31"/>
<instance part="GND10" gate="1" x="82.55" y="87.63"/>
<instance part="P+2" gate="1" x="93.98" y="142.24" smashed="yes">
<attribute name="VALUE" x="92.71" y="144.78" size="1.778" layer="96"/>
</instance>
<instance part="P+3" gate="1" x="27.94" y="124.46" rot="R90"/>
<instance part="T2" gate="G$1" x="91.44" y="76.2" smashed="yes">
<attribute name="NAME" x="81.28" y="80.01" size="1.778" layer="95"/>
<attribute name="VALUE" x="81.28" y="77.47" size="1.778" layer="96"/>
</instance>
<instance part="T1" gate="G$1" x="80.01" y="96.52" smashed="yes">
<attribute name="NAME" x="71.12" y="100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="71.12" y="97.79" size="1.778" layer="96"/>
</instance>
<instance part="T3" gate="G$1" x="104.14" y="55.88" smashed="yes">
<attribute name="NAME" x="93.98" y="59.69" size="1.778" layer="95"/>
<attribute name="VALUE" x="93.98" y="57.15" size="1.778" layer="96"/>
</instance>
<instance part="R20" gate="G$1" x="82.55" y="107.95" rot="R90"/>
<instance part="R21" gate="G$1" x="93.98" y="88.9" rot="R90"/>
<instance part="R22" gate="G$1" x="106.68" y="68.58" rot="R90"/>
<instance part="R23" gate="G$1" x="93.98" y="132.08" rot="R270"/>
<instance part="R26" gate="G$1" x="63.5" y="76.2"/>
<instance part="R27" gate="G$1" x="63.5" y="55.88"/>
<instance part="GND11" gate="1" x="106.68" y="46.99"/>
<instance part="F2" gate="G$1" x="0" y="0"/>
<instance part="F2" gate="G$2" x="162.56" y="0"/>
<instance part="R24" gate="G$1" x="63.5" y="62.23"/>
<instance part="GND" gate="G$1" x="213.36" y="55.88" rot="R90"/>
<instance part="GND13" gate="1" x="213.36" y="49.53"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="36.83" y1="154.94" x2="34.29" y2="154.94" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="184.15" y1="110.49" x2="186.69" y2="110.49" width="0.1524" layer="91"/>
<wire x1="186.69" y1="110.49" x2="187.96" y2="110.49" width="0.1524" layer="91"/>
<wire x1="187.96" y1="107.95" x2="186.69" y2="107.95" width="0.1524" layer="91"/>
<wire x1="186.69" y1="107.95" x2="186.69" y2="110.49" width="0.1524" layer="91"/>
<pinref part="PCB1" gate="G$1" pin="GND."/>
<pinref part="PCB1" gate="G$1" pin="GND.."/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="231.14" y1="125.73" x2="234.95" y2="125.73" width="0.1524" layer="91"/>
<pinref part="PCB1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="LCD1" gate="G$1" pin="PSB"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="36.83" y1="134.62" x2="34.29" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LCD1" gate="G$1" pin="R/!W!"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="67.31" y1="134.62" x2="76.2" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="149.86" y1="153.67" x2="149.86" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="149.86" y1="125.7554" x2="149.86" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="149.86" y1="95.25" x2="149.86" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="149.86" y1="66.04" x2="149.86" y2="67.31" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="149.86" y1="36.83" x2="149.86" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="T1" gate="G$1" pin="E"/>
<wire x1="82.55" y1="91.44" x2="82.55" y2="90.17" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="T2" gate="G$1" pin="E"/>
<wire x1="93.98" y1="71.12" x2="93.98" y2="69.85" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="T3" gate="G$1" pin="E"/>
<wire x1="106.68" y1="50.8" x2="106.68" y2="49.53" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="213.36" y1="52.07" x2="213.36" y2="55.88" width="0.1524" layer="91"/>
<pinref part="GND" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SI" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="D7"/>
<wire x1="67.31" y1="139.7" x2="76.2" y2="139.7" width="0.1524" layer="91"/>
<label x="71.12" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="144.78" y1="107.95" x2="149.86" y2="107.95" width="0.1524" layer="91"/>
<wire x1="149.86" y1="106.68" x2="149.86" y2="107.95" width="0.1524" layer="91"/>
<label x="152.4" y="109.22" size="1.778" layer="95"/>
<wire x1="149.86" y1="107.95" x2="158.75" y2="107.95" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="R8" gate="G$1" pin="2"/>
<junction x="149.86" y="107.95"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="D6"/>
<wire x1="67.31" y1="142.24" x2="76.2" y2="142.24" width="0.1524" layer="91"/>
<label x="71.12" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="144.78" y1="137.16" x2="149.86" y2="137.16" width="0.1524" layer="91"/>
<wire x1="149.86" y1="135.89" x2="149.86" y2="135.9154" width="0.1524" layer="91"/>
<label x="152.4" y="138.43" size="1.778" layer="95"/>
<wire x1="149.86" y1="135.9154" x2="149.86" y2="137.16" width="0.1524" layer="91"/>
<wire x1="149.86" y1="137.16" x2="158.75" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="R6" gate="G$1" pin="2"/>
<junction x="149.86" y="137.16"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="!CSB!"/>
<wire x1="67.31" y1="129.54" x2="76.2" y2="129.54" width="0.1524" layer="91"/>
<label x="71.12" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="144.78" y1="49.53" x2="149.86" y2="49.53" width="0.1524" layer="91"/>
<wire x1="149.86" y1="48.26" x2="149.86" y2="49.53" width="0.1524" layer="91"/>
<label x="152.4" y="50.8" size="1.778" layer="95"/>
<wire x1="149.86" y1="49.53" x2="157.48" y2="49.53" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="R12" gate="G$1" pin="2"/>
<junction x="149.86" y="49.53"/>
</segment>
</net>
<net name="RS" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="RS"/>
<wire x1="67.31" y1="137.16" x2="76.2" y2="137.16" width="0.1524" layer="91"/>
<label x="71.12" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="144.78" y1="78.74" x2="149.86" y2="78.74" width="0.1524" layer="91"/>
<wire x1="149.86" y1="77.47" x2="149.86" y2="78.74" width="0.1524" layer="91"/>
<label x="152.4" y="80.01" size="1.778" layer="95"/>
<wire x1="149.86" y1="78.74" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="R10" gate="G$1" pin="2"/>
<junction x="149.86" y="78.74"/>
</segment>
</net>
<net name="VDD_LCD" class="0">
<segment>
<wire x1="74.93" y1="165.1" x2="73.66" y2="165.1" width="0.1524" layer="91"/>
<wire x1="73.66" y1="165.1" x2="73.66" y2="160.02" width="0.1524" layer="91"/>
<wire x1="73.66" y1="160.02" x2="74.93" y2="160.02" width="0.1524" layer="91"/>
<wire x1="15.3162" y1="142.24" x2="15.3162" y2="140.97" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="VIN"/>
<wire x1="36.83" y1="142.24" x2="15.3162" y2="142.24" width="0.1524" layer="91"/>
<wire x1="15.3162" y1="142.24" x2="15.3162" y2="165.1" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="VDD"/>
<wire x1="36.83" y1="157.48" x2="35.56" y2="157.48" width="0.1524" layer="91"/>
<wire x1="35.56" y1="157.48" x2="35.56" y2="165.1" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="E"/>
<wire x1="67.31" y1="132.08" x2="69.85" y2="132.08" width="0.1524" layer="91"/>
<wire x1="69.85" y1="132.08" x2="69.85" y2="144.78" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="D0"/>
<wire x1="69.85" y1="144.78" x2="69.85" y2="147.32" width="0.1524" layer="91"/>
<wire x1="69.85" y1="147.32" x2="69.85" y2="149.86" width="0.1524" layer="91"/>
<wire x1="69.85" y1="149.86" x2="69.85" y2="152.4" width="0.1524" layer="91"/>
<wire x1="69.85" y1="152.4" x2="69.85" y2="154.94" width="0.1524" layer="91"/>
<wire x1="69.85" y1="154.94" x2="69.85" y2="157.48" width="0.1524" layer="91"/>
<wire x1="67.31" y1="157.48" x2="69.85" y2="157.48" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="D1"/>
<wire x1="67.31" y1="154.94" x2="69.85" y2="154.94" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="D2"/>
<wire x1="67.31" y1="152.4" x2="69.85" y2="152.4" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="D3"/>
<wire x1="67.31" y1="149.86" x2="69.85" y2="149.86" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="D4"/>
<wire x1="67.31" y1="147.32" x2="69.85" y2="147.32" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="D5"/>
<wire x1="67.31" y1="144.78" x2="69.85" y2="144.78" width="0.1524" layer="91"/>
<wire x1="35.56" y1="165.1" x2="69.85" y2="165.1" width="0.1524" layer="91"/>
<wire x1="69.85" y1="165.1" x2="69.85" y2="157.48" width="0.1524" layer="91"/>
<wire x1="15.3162" y1="165.1" x2="35.56" y2="165.1" width="0.1524" layer="91"/>
<wire x1="73.66" y1="165.1" x2="69.85" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<junction x="69.85" y="157.48"/>
<junction x="69.85" y="154.94"/>
<junction x="69.85" y="152.4"/>
<junction x="69.85" y="149.86"/>
<junction x="69.85" y="147.32"/>
<junction x="69.85" y="144.78"/>
<junction x="35.56" y="165.1"/>
<junction x="73.66" y="165.1"/>
<junction x="69.85" y="165.1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="20.32" y1="156.21" x2="20.32" y2="157.48" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="CAP1N"/>
<wire x1="36.83" y1="149.86" x2="25.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="25.4" y1="149.86" x2="25.4" y2="157.48" width="0.1524" layer="91"/>
<wire x1="25.4" y1="157.48" x2="20.32" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="20.32" y1="148.59" x2="20.32" y2="147.32" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="CAP1P"/>
<wire x1="36.83" y1="147.32" x2="20.32" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="15.3162" y1="133.35" x2="15.3162" y2="132.08" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="VOUT"/>
<wire x1="36.83" y1="139.7" x2="22.9616" y2="139.7" width="0.1524" layer="91"/>
<wire x1="22.9616" y1="139.7" x2="22.9616" y2="132.08" width="0.1524" layer="91"/>
<wire x1="22.9616" y1="132.08" x2="15.3162" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="85.09" y1="165.1" x2="86.36" y2="165.1" width="0.1524" layer="91"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="PCB1" gate="G$1" pin="3.3V"/>
<wire x1="171.45" y1="115.57" x2="187.96" y2="115.57" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="86.36" y1="160.02" x2="85.09" y2="160.02" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<pinref part="R2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="PCB1" gate="G$1" pin="5V"/>
<pinref part="+5V" gate="1" pin="+5V"/>
<wire x1="187.96" y1="113.03" x2="176.53" y2="113.03" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="A1"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="36.83" y1="124.46" x2="30.48" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="93.98" y1="137.16" x2="93.98" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<wire x1="144.78" y1="166.37" x2="149.86" y2="166.37" width="0.1524" layer="91"/>
<wire x1="149.86" y1="165.1" x2="149.86" y2="166.37" width="0.1524" layer="91"/>
<label x="152.4" y="167.64" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="149.86" y1="166.37" x2="160.02" y2="166.37" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<junction x="149.86" y="166.37"/>
</segment>
<segment>
<pinref part="LCD1" gate="G$1" pin="!RESET!"/>
<wire x1="36.83" y1="129.54" x2="22.86" y2="129.54" width="0.1524" layer="91"/>
<label x="25.4" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="CS_5V" class="0">
<segment>
<wire x1="134.62" y1="49.53" x2="123.19" y2="49.53" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<label x="124.46" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PCB1" gate="G$1" pin="~10"/>
<wire x1="231.14" y1="115.57" x2="250.19" y2="115.57" width="0.1524" layer="91"/>
<label x="234.95" y="115.57" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS_5V" class="0">
<segment>
<wire x1="134.62" y1="78.74" x2="123.19" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<label x="124.46" y="80.01" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PCB1" gate="G$1" pin="~9"/>
<wire x1="231.14" y1="113.03" x2="250.19" y2="113.03" width="0.1524" layer="91"/>
<label x="234.95" y="113.03" size="1.778" layer="95"/>
</segment>
</net>
<net name="SI_5V" class="0">
<segment>
<wire x1="134.62" y1="107.95" x2="123.19" y2="107.95" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<label x="124.46" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PCB1" gate="G$1" pin="~11"/>
<wire x1="231.14" y1="118.11" x2="250.19" y2="118.11" width="0.1524" layer="91"/>
<label x="234.95" y="118.11" size="1.778" layer="95"/>
</segment>
</net>
<net name="CLK_5V" class="0">
<segment>
<wire x1="134.62" y1="137.16" x2="123.19" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<label x="124.46" y="138.43" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PCB1" gate="G$1" pin="13"/>
<wire x1="231.14" y1="123.19" x2="255.27" y2="123.19" width="0.1524" layer="91"/>
<label x="243.84" y="123.19" size="1.778" layer="95"/>
</segment>
</net>
<net name="RESET_5V" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="134.62" y1="166.37" x2="119.38" y2="166.37" width="0.1524" layer="91"/>
<label x="120.65" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PCB1" gate="G$1" pin="4"/>
<wire x1="231.14" y1="95.25" x2="250.19" y2="95.25" width="0.1524" layer="91"/>
<label x="234.95" y="95.25" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="C1"/>
<wire x1="36.83" y1="121.92" x2="35.56" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="82.55" y1="113.03" x2="82.55" y2="116.84" width="0.1524" layer="91"/>
<wire x1="82.55" y1="116.84" x2="35.56" y2="116.84" width="0.1524" layer="91"/>
<wire x1="35.56" y1="116.84" x2="35.56" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="T1" gate="G$1" pin="C"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="82.55" y1="101.6" x2="82.55" y2="102.87" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="LED1" gate="G$1" pin="A2"/>
<wire x1="93.98" y1="93.98" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<wire x1="93.98" y1="121.92" x2="67.31" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="93.98" y1="127" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<junction x="93.98" y="121.92"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="LED1" gate="G$1" pin="C2"/>
<wire x1="106.68" y1="73.66" x2="106.68" y2="124.46" width="0.1524" layer="91"/>
<wire x1="106.68" y1="124.46" x2="67.31" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="T2" gate="G$1" pin="C"/>
<wire x1="93.98" y1="83.82" x2="93.98" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="T3" gate="G$1" pin="C"/>
<wire x1="106.68" y1="63.5" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="T1" gate="G$1" pin="B"/>
<wire x1="68.58" y1="96.52" x2="77.47" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="T2" gate="G$1" pin="B"/>
<wire x1="68.58" y1="76.2" x2="88.9" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="T3" gate="G$1" pin="B"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="101.6" y1="55.88" x2="76.2" y2="55.88" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="76.2" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<wire x1="68.58" y1="62.23" x2="76.2" y2="62.23" width="0.1524" layer="91"/>
<wire x1="76.2" y1="62.23" x2="76.2" y2="55.88" width="0.1524" layer="91"/>
<junction x="76.2" y="55.88"/>
</segment>
</net>
<net name="R_RED/MONO" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="58.42" y1="96.52" x2="52.07" y2="96.52" width="0.1524" layer="91"/>
<label x="34.29" y="97.79" size="1.778" layer="95"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="52.07" y1="96.52" x2="31.75" y2="96.52" width="0.1524" layer="91"/>
<wire x1="52.07" y1="96.52" x2="52.07" y2="62.23" width="0.1524" layer="91"/>
<wire x1="52.07" y1="62.23" x2="58.42" y2="62.23" width="0.1524" layer="91"/>
<junction x="52.07" y="96.52"/>
</segment>
<segment>
<pinref part="PCB1" gate="G$1" pin="~3"/>
<wire x1="231.14" y1="92.71" x2="250.19" y2="92.71" width="0.1524" layer="91"/>
<label x="234.95" y="92.71" size="1.778" layer="95"/>
</segment>
</net>
<net name="G_GREEN" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="58.42" y1="76.2" x2="31.75" y2="76.2" width="0.1524" layer="91"/>
<label x="34.29" y="77.47" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PCB1" gate="G$1" pin="~5"/>
<wire x1="231.14" y1="97.79" x2="250.19" y2="97.79" width="0.1524" layer="91"/>
<label x="234.95" y="97.79" size="1.778" layer="95"/>
</segment>
</net>
<net name="B_BLUE" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="58.42" y1="55.88" x2="31.75" y2="55.88" width="0.1524" layer="91"/>
<label x="34.29" y="57.15" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PCB1" gate="G$1" pin="~6"/>
<wire x1="231.14" y1="100.33" x2="250.19" y2="100.33" width="0.1524" layer="91"/>
<label x="234.95" y="100.33" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
