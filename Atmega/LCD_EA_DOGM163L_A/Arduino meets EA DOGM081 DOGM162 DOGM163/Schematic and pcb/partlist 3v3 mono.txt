Partlist

Exported from Arduino meets EA DOGM081 DOGM162 DOGM163 Rev_A.brd at 25.06.2014 11:28:44

EAGLE Version 6.5.0 Copyright (c) 1988-2013 CadSoft

Assembly variant: 3.3V monochrome

Part     Value              Package            Library        Position (mm)         Orientation

C1       1uF                C_5MM_AND_1206     EA             (45.72 41.275)        R180
C3       1uF                C_5MM_AND_1206     EA             (48.895 41.275)       R0
GND      PAD55/35           PAD55/35           EA             (22.225 2.8575)       R0
LCD1     EA DOGMxxxx-A      EA_DOGM            EA             (3.81 10.16)          R0
LED1     EA LED55X31-xxx    EA_LED55X31        EA             (3.81 10.16)          R0
PCB1     ARDUINO_UNO_SHIELD ARDUINO_UNO_SHIELD E14_Arduino    (0 0)                 R0
R1       0R                 R_10MM_AND_1206    EA             (27.305 6.35)         R0
R3       2k7                R_10MM_AND_1206    EA             (1.905 51.435)        R270
R4       4k7                R_10MM_AND_1206    EA             (4.7625 51.435)       R270
R5       2k7                R_10MM_AND_1206    EA             (41.275 47.3075)      R0
R6       4k7                R_10MM_AND_1206    EA             (41.275 44.45)        R0
R7       2k7                R_10MM_AND_1206    EA             (27.305 46.0375)      R0
R8       4k7                R_10MM_AND_1206    EA             (27.305 43.18)        R0
R9       2k7                R_10MM_AND_1206    EA             (7.62 51.435)         R270
R10      4k7                R_10MM_AND_1206    EA             (10.4775 51.435)      R270
R11      2k7                R_10MM_AND_1206    EA             (24.13 46.0375)       R180
R12      4k7                R_10MM_AND_1206    EA             (24.13 43.18)         R180
R20      R_10MM_AND_1206    R_10MM_AND_1206    EA             (61.2775 21.9075)     R270
R22      R_10MM_AND_1206    R_10MM_AND_1206    EA             (66.9925 21.9075)     R270
R23      0R                 R_10MM_AND_1206    EA             (47.625 6.35)         R0
R24      470R               R_10MM_AND_1206    EA             (54.61 48.26)         R0
R25      470R               R_10MM_AND_1206    EA             (57.15 46.0375)       R270
T1       BC547              TO92               transistor-npn (61.2775 26.035)      R90
T3       BC547              TO92               transistor-npn (66.3575 26.035)      R90
